package com.example.rushhourtrafficcontrolpredictor.Controller;

import com.example.rushhourtrafficcontrolpredictor.Model.Plate;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class RushHourManagerTest {

    @Test
    public void checkPlateEndingInOne() throws ParseException {
        Plate plate = new Plate("AAA-0001");
        Assert.assertFalse("End in 1 road on Monday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("14-09-2020")));
        Assert.assertTrue("End in 1 road on Tuesday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("15-09-2020")));
        Assert.assertTrue("End in 1 road on Wednesday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("16-09-2020")));
        Assert.assertTrue("End in 1 road on Thursday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("17-09-2020")));
        Assert.assertTrue("End in 1 road on Friday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("18-09-2020")));
        Assert.assertTrue("End in 1 road on Saturday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("19-09-2020")));
        Assert.assertTrue("End in 1 road on Sunday", RushHourManager.getInstance().isAllowedToRoad(plate,new SimpleDateFormat("dd-MM-yyyy").parse("20-09-2020")));
    }

    @Test
    public void checkPlateEndingInTwo() throws ParseException {
        Plate plate = new Plate("AAA-0002");
        Assert.assertFalse("End in 2 road on Monday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("14-09-2020")));
        Assert.assertTrue("End in 2 road on Tuesday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("15-09-2020")));
        Assert.assertTrue("End in 2 road on Wednesday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("16-09-2020")));
        Assert.assertTrue("End in 2 road on Thursday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("17-09-2020")));
        Assert.assertTrue("End in 2 road on Friday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("18-09-2020")));
        Assert.assertTrue("End in 2 road on Saturday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("19-09-2020")));
        Assert.assertTrue("End in 2 road on Sunday", RushHourManager.getInstance().isAllowedToRoad(plate,new SimpleDateFormat("dd-MM-yyyy").parse("20-09-2020")));
    }

    @Test
    public void checkPlateEndingInThree() throws ParseException {
        Plate plate = new Plate("AAA-0003");
        Assert.assertTrue("End in 3 road on Monday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("14-09-2020")));
        Assert.assertFalse("End in 3 road on Tuesday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("15-09-2020")));
        Assert.assertTrue("End in 3 road on Wednesday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("16-09-2020")));
        Assert.assertTrue("End in 3 road on Thursday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("17-09-2020")));
        Assert.assertTrue("End in 3 road on Friday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("18-09-2020")));
        Assert.assertTrue("End in 3 road on Saturday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("19-09-2020")));
        Assert.assertTrue("End in 3 road on Sunday", RushHourManager.getInstance().isAllowedToRoad(plate,new SimpleDateFormat("dd-MM-yyyy").parse("20-09-2020")));
    }

    @Test
    public void checkPlateEndingInFour() throws ParseException {
        Plate plate = new Plate("AAA-0004");
        Assert.assertTrue("End in 4 road on Monday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("14-09-2020")));
        Assert.assertFalse("End in 4 road on Tuesday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("15-09-2020")));
        Assert.assertTrue("End in 4 road on Wednesday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("16-09-2020")));
        Assert.assertTrue("End in 4 road on Thursday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("17-09-2020")));
        Assert.assertTrue("End in 4 road on Friday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("18-09-2020")));
        Assert.assertTrue("End in 4 road on Saturday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("19-09-2020")));
        Assert.assertTrue("End in 4 road on Sunday", RushHourManager.getInstance().isAllowedToRoad(plate,new SimpleDateFormat("dd-MM-yyyy").parse("20-09-2020")));
    }

    @Test
    public void checkPlateEndingInFive() throws ParseException {
        Plate plate = new Plate("AAA-0005");
        Assert.assertTrue("End in 5 road on Monday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("14-09-2020")));
        Assert.assertTrue("End in 5 road on Tuesday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("15-09-2020")));
        Assert.assertFalse("End in 5 road on Wednesday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("16-09-2020")));
        Assert.assertTrue("End in 5 road on Thursday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("17-09-2020")));
        Assert.assertTrue("End in 5 road on Friday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("18-09-2020")));
        Assert.assertTrue("End in 5 road on Saturday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("19-09-2020")));
        Assert.assertTrue("End in 5 road on Sunday", RushHourManager.getInstance().isAllowedToRoad(plate,new SimpleDateFormat("dd-MM-yyyy").parse("20-09-2020")));
    }

    @Test
    public void checkPlateEndingInSix() throws ParseException {
        Plate plate = new Plate("AAA-0006");
        Assert.assertTrue("End in 6 road on Monday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("14-09-2020")));
        Assert.assertTrue("End in 6 road on Tuesday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("15-09-2020")));
        Assert.assertFalse("End in 6 road on Wednesday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("16-09-2020")));
        Assert.assertTrue("End in 6 road on Thursday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("17-09-2020")));
        Assert.assertTrue("End in 6 road on Friday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("18-09-2020")));
        Assert.assertTrue("End in 6 road on Saturday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("19-09-2020")));
        Assert.assertTrue("End in 6 road on Sunday", RushHourManager.getInstance().isAllowedToRoad(plate,new SimpleDateFormat("dd-MM-yyyy").parse("20-09-2020")));
    }

    @Test
    public void checkPlateEndingInSeven() throws ParseException {
        Plate plate = new Plate("AAA-0007");
        Assert.assertTrue("End in 7 road on Monday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("14-09-2020")));
        Assert.assertTrue("End in 7 road on Tuesday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("15-09-2020")));
        Assert.assertTrue("End in 7 road on Wednesday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("16-09-2020")));
        Assert.assertFalse("End in 7 road on Thursday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("17-09-2020")));
        Assert.assertTrue("End in 7 road on Friday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("18-09-2020")));
        Assert.assertTrue("End in 7 road on Saturday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("19-09-2020")));
        Assert.assertTrue("End in 7 road on Sunday", RushHourManager.getInstance().isAllowedToRoad(plate,new SimpleDateFormat("dd-MM-yyyy").parse("20-09-2020")));
    }

    @Test
    public void checkPlateEndingInEight() throws ParseException {
        Plate plate = new Plate("AAA-0008");
        Assert.assertTrue("End in 8 road on Monday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("14-09-2020")));
        Assert.assertTrue("End in 8 road on Tuesday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("15-09-2020")));
        Assert.assertTrue("End in 8 road on Wednesday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("16-09-2020")));
        Assert.assertFalse("End in 8 road on Thursday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("17-09-2020")));
        Assert.assertTrue("End in 8 road on Friday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("18-09-2020")));
        Assert.assertTrue("End in 8 road on Saturday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("19-09-2020")));
        Assert.assertTrue("End in 8 road on Sunday", RushHourManager.getInstance().isAllowedToRoad(plate,new SimpleDateFormat("dd-MM-yyyy").parse("20-09-2020")));
    }

    @Test
    public void checkPlateEndingInNine() throws ParseException {
        Plate plate = new Plate("AAA-0009");
        Assert.assertTrue("End in 9 road on Monday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("14-09-2020")));
        Assert.assertTrue("End in 9 road on Tuesday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("15-09-2020")));
        Assert.assertTrue("End in 9 road on Wednesday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("16-09-2020")));
        Assert.assertTrue("End in 9 road on Thursday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("17-09-2020")));
        Assert.assertFalse("End in 9 road on Friday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("18-09-2020")));
        Assert.assertTrue("End in 9 road on Saturday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("19-09-2020")));
        Assert.assertTrue("End in 9 road on Sunday", RushHourManager.getInstance().isAllowedToRoad(plate,new SimpleDateFormat("dd-MM-yyyy").parse("20-09-2020")));
    }

    @Test
    public void checkPlateEndingInZero() throws ParseException {
        Plate plate = new Plate("AAA-0000");
        Assert.assertTrue("End in 0 road on Monday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("14-09-2020")));
        Assert.assertTrue("End in 0 road on Tuesday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("15-09-2020")));
        Assert.assertTrue("End in 0 road on Wednesday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("16-09-2020")));
        Assert.assertTrue("End in 0 road on Thursday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("17-09-2020")));
        Assert.assertFalse("End in 0 road on Friday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("18-09-2020")));
        Assert.assertTrue("End in 0 road on Saturday", RushHourManager.getInstance().isAllowedToRoad(plate, new SimpleDateFormat("dd-MM-yyyy").parse("19-09-2020")));
        Assert.assertTrue("End in 0 road on Sunday", RushHourManager.getInstance().isAllowedToRoad(plate,new SimpleDateFormat("dd-MM-yyyy").parse("20-09-2020")));
    }
}
