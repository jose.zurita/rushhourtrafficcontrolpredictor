package com.example.rushhourtrafficcontrolpredictor.Controller;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

public class ValidatorTest {
    @Test
    public void checkValidPlate(){
        String plate = "AAA-0000";
        Assert.assertTrue("Three number plate", Validator.getInstance().isValidFullPlate(plate));
        plate = "AAA-0000";
        Assert.assertTrue("Four number plate", Validator.getInstance().isValidFullPlate(plate));
        plate = "aNc-1241";
        Assert.assertTrue("Capital letters", Validator.getInstance().isValidFullPlate(plate));
    }

    @Test
    public void checkInvalidPlate() {
        String plate = null;
        Assert.assertFalse("Null input", Validator.getInstance().isValidFullPlate(plate));
        plate = "";
        Assert.assertFalse("Empty string", Validator.getInstance().isValidFullPlate(plate));
        plate = "asdsafasfadsa";
        Assert.assertFalse("Nonsense", Validator.getInstance().isValidFullPlate(plate));
        plate = "asdsadsa-545";
        Assert.assertFalse("Something before plate", Validator.getInstance().isValidFullPlate(plate));
        plate = "dsa-54512a";
        Assert.assertFalse("Something before plate", Validator.getInstance().isValidFullPlate(plate));
        plate = "áNc-1241";
        Assert.assertFalse("Special characters", Validator.getInstance().isValidFullPlate(plate));
    }

    @Test
    public void checkValidDate() {
        String date = "16-09-2020";
        Assert.assertTrue("Date with right format", Validator.getInstance().isValidDate(date));
        date = "01-09-2020";
        Assert.assertTrue("Another date with right format", Validator.getInstance().isValidDate(date));
    }

    @Test
    public void checkInvalidDate() {
        String date = null;
        Assert.assertFalse("Null input", Validator.getInstance().isValidDate(date));
        date = "";
        Assert.assertFalse("Empty string", Validator.getInstance().isValidDate(date));
        date = "asfasfsaf";
        Assert.assertFalse("Nonsense", Validator.getInstance().isValidDate(date));
        date = "12/05/20";
        Assert.assertFalse("Date with different format", Validator.getInstance().isValidDate(date));
        date = "01-17-2020";
        Assert.assertFalse("Date with 13 as month", Validator.getInstance().isValidDate(date));
    }
}
