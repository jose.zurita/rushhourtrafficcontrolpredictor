package com.example.rushhourtrafficcontrolpredictor.Rules;

public enum ForbiddenPlatesForEachDay {
    MONDAY ("1", "2"),
    TUESDAY ("3", "4"),
    WEDNESDAY ("5","6"),
    THURSDAY ("7","8"),
    FRIDAY ("9","0"),
    SATURDAY (),
    SUNDAY ();

    private final String[] forbiddenDigits;

    ForbiddenPlatesForEachDay(String ... forbiddenDigits) {
        this.forbiddenDigits = forbiddenDigits;
    }

    public String[] getForbiddenDigits() {
        return this.forbiddenDigits;
    }
}
