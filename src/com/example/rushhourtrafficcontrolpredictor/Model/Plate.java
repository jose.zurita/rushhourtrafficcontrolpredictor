package com.example.rushhourtrafficcontrolpredictor.Model;

import com.example.rushhourtrafficcontrolpredictor.Controller.Validator;

public class Plate {
    private String alphabeticPart;
    private String numericPart;

    public Plate (String fullPlate) {
        if(Validator.getInstance().isValidFullPlate(fullPlate)){
            String [] plateParts = fullPlate.split("-");
            this.alphabeticPart = plateParts[0];
            this.numericPart = plateParts[1];
        }
    }

    public Plate(String alphabeticPart, String numericPart) {
        this.alphabeticPart = alphabeticPart;
        this.numericPart = numericPart;
    }

    public String getNumericPart() {
        return numericPart;
    }

    public void setNumericPart(String numericPart) {
        this.numericPart = numericPart;
    }

    public String getAlphabeticPart() {
        return alphabeticPart;
    }

    public void setAlphabeticPart(String alphabeticPart) {
        this.alphabeticPart = alphabeticPart;
    }

    public String getFullPlate() {
        return (alphabeticPart != null && numericPart != null) ? alphabeticPart + "-" + numericPart : null;
    }

    public String getLastPlateDigit() {
        return (numericPart != null) ? String.valueOf(numericPart.charAt(numericPart.length() -1)) : null;
    }
}
