package com.example.rushhourtrafficcontrolpredictor.View;

import com.example.rushhourtrafficcontrolpredictor.Controller.RushHourManager;
import com.example.rushhourtrafficcontrolpredictor.Controller.Validator;
import com.example.rushhourtrafficcontrolpredictor.Model.Plate;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

public class Console {

    public static void main (String [] arguments) throws Exception {
        String inputPlate;
        String inputDate;
        Plate carPlate;
        Date evaluationDate;

        if (arguments.length == 0) {
            System.out.println("Welcome to the Rush hour traffic control predictor\n");
            System.out.println("This application will help you to find out if your car is allowed to road on the given date\n");
            System.out.println("Please enter your car's plate:");
            Scanner input = new Scanner(System.in);
            inputPlate = input.nextLine();
            while (!Validator.getInstance().isValidFullPlate(inputPlate)) {
                System.out.println("Sorry the plate format is not correct. It should be something like AAA-0000:");
                inputPlate = input.nextLine();
            }
            System.out.println("Please enter the date you want to want to find out whether you can road or not (dd-MM-yyyy):");
            inputDate = input.nextLine();
            while (!Validator.getInstance().isValidDate(inputDate)) {
                System.out.println("Sorry the date format is not correct. It should be dd-mm-yyyy:");
                inputDate = input.nextLine();
            }
        }
        else {
            if(arguments.length != 2) {
                throw new Exception("Unable to run with " + arguments.length + " parameters. Expected 2 (plate and date).");
            }
            if(!Validator.getInstance().isValidFullPlate(arguments[0])) {
                throw new Exception("The plate format is not correct. It should be something like AAA-0000");
            }
            if(!Validator.getInstance().isValidDate(arguments[1])) {
                throw new Exception("The date format is not correct. It should be dd-mm-yyyy");
            }
            inputPlate = arguments[0];
            inputDate = arguments[1];
        }

        carPlate = new Plate(inputPlate);
        evaluationDate = new SimpleDateFormat("dd-MM-yyyy").parse(inputDate);

        if(RushHourManager.getInstance().isAllowedToRoad(carPlate, evaluationDate)) {
            System.out.println("On " + new SimpleDateFormat("E, dd MMM yyyy", Locale.ENGLISH).format(evaluationDate) + ", the car with the plate " + carPlate.getFullPlate() + " is allowed to road");
        }
        else {
            System.out.println("On " + new SimpleDateFormat("E, dd MMM yyyy", Locale.ENGLISH).format(evaluationDate) + ", the car with the plate " + carPlate.getFullPlate() + " is allowed to road");
        }
    }
}