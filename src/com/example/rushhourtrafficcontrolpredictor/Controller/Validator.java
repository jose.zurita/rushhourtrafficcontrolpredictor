package com.example.rushhourtrafficcontrolpredictor.Controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Formatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator {
    private static Validator validatorInstance;

    private Validator() {}

    public static Validator getInstance() {
        return (validatorInstance != null) ? validatorInstance : new Validator();
    }

    public boolean isValidFullPlate (String fullPlate) {
        if (fullPlate == null) { return false; }
        Pattern pattern = Pattern.compile("^[A-Z]{3}-\\d{3,4}$", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(fullPlate);
        return matcher.find();
    }

    public boolean isValidDate(String inputDate) {
        DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        formatter.setLenient(false);
        try {
            formatter.parse(inputDate);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
