package com.example.rushhourtrafficcontrolpredictor.Controller;

import com.example.rushhourtrafficcontrolpredictor.Model.Plate;
import com.example.rushhourtrafficcontrolpredictor.Rules.ForbiddenPlatesForEachDay;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

public class RushHourManager {
    private static RushHourManager managerInstance;

    private RushHourManager() {}

    public static RushHourManager getInstance() {
        return (managerInstance != null) ? managerInstance : new RushHourManager();
    }

    public boolean isAllowedToRoad (Plate carPlate, Date evaluationDate) {
        String evaluationDayOfTheWeek = getDayOfTheWeek(evaluationDate);
        String [] forbiddenPlatesForEvaluationDate = ForbiddenPlatesForEachDay.valueOf(evaluationDayOfTheWeek).getForbiddenDigits();
        return !Arrays.asList(forbiddenPlatesForEvaluationDate).contains(carPlate.getLastPlateDigit());
    }

    public static String getDayOfTheWeek (Date date) {
        DateFormat formatter = new SimpleDateFormat("EEEE", Locale.ENGLISH);
        return formatter.format(date).toUpperCase();
    }
}
